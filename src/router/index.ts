
import { createRouter, createWebHistory } from 'vue-router';
import GarageView from '../views/GarageView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
  {
    path: '/',
    redirect: '/garage'
  },
  {
    path: '/garage',
    name: 'garage',
    component: GarageView
  },
  {
    path: '/news',
    name: 'news',
    component: () => import('../views/NewsView.vue')
  },
  {
    path: '/cal',
    name: 'cal',
    component: () => import('../views/CalculatorView.vue')
  },
  {
    path: '/carparts',
    name: 'carparts',
    component: () => import('../views/CarPartsView.vue')
  },
  {
    path: '/About',
    name: 'About',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/carloan',
    name: 'carloan',
    component: () => import('../views/CalcarloanView.vue')
  },
  {
    path: '/cartax',
    name: 'cartax',
    component: () => import('../views/CaltaxView.vue')
  },
  {
    path: '/caroil',
    name: 'caroil',
    component: () => import('../views/CalcaroilView.vue')
  },
  {
    path: '/sell',
    name: 'sell',
    component: () => import('../views/SellCarView.vue')
  },
  {
    path: '/footer',
    name: 'footer',
    component: () => import('../views/FooterView.vue')
  },

]
});

router.replace({ name: 'garage' })

export default router;